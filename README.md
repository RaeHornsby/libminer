
<!-- README.md is generated from README.Rmd. Please edit that file -->

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overview of your R library setup.
It is a toy and not meant for serious use.

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab("boshek/libminer")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(libminer)
lib_summary()
#>                                                                   Library
#> 1                                      C:/Program Files/R/R-4.3.2/library
#> 2                        C:/Users/hornsby/AppData/Local/R/win-library/4.3
#> 3 C:/Users/hornsby/AppData/Local/Temp/RtmpktmOim/temp_libpath71d4786d577a
#>   n_packages
#> 1         30
#> 2        188
#> 3          1

#Or to calculate sizes

lib_summary(sizes = TRUE)
#>                                                                   Library
#> 1                                      C:/Program Files/R/R-4.3.2/library
#> 2                        C:/Users/hornsby/AppData/Local/R/win-library/4.3
#> 3 C:/Users/hornsby/AppData/Local/Temp/RtmpktmOim/temp_libpath71d4786d577a
#>   n_packages  lib_size
#> 1         30  69129945
#> 2        188 572957394
#> 3          1     12646
```
